
# there are some usefull scripts for dovecote
    - change-param  => for rotate the used param for dovecot (recommend)
    - new-user      => add user to /etc/dovecot/users
    - changepw      => change the pw + hash algorythmus for the user
    
## How to use
    - You can now use the scripts as API (take a look in help with -h).
    - You can use the scripts manually also.
    
### like:
    - help: --help / -h 
        -----------------------------------------------------------------------------------
        1) Insert username:    -u \$user
        -----------------------------------------------------------------------------------
        2) Insert hash: -hs \$hash
        
        Possebilities for hashes: 
            
            PLAIN     : PLAIN sum of the password stored in plaintext
            SHA       : SHA1 sum of the password stored in base64
            SSHA      : Salted SHA1 sum of the password stored in base64
            SHA256    : SHA256 sum of the password stored in base64. (v1.1 and later)
            SSHA256   : Salted SHA256 sum of the password stored in base64. (v1.2 and later)
            SHA512    : SHA512 sum of the password stored in base64. (v2.0 and later)
            SSHA512   : Salted SHA512 sum of the password stored in base64. (v2.0 and later)
        -----------------------------------------------------------------------------------
        3) Password :    -p \$password
        -----------------------------------------------------------------------------------
        -----------------------------------------------------------------------------------
         Default order for the inserts:
         1) username
         2) hash
         3) password
        
        for example: /bin/bash new-user.sh -u \$username -hs \$hash -p \$password
        
        Note: you can also switch the inserts for 1-3
        -----------------------------------------------------------------------------------
        
