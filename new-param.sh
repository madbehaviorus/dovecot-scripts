#!/bin/bash

## use new param

# new screen
screen -S new-param

# generate new dhparam
openssl dhparam -out /etc/postfix/dh_4096.pem2 -2 4096
openssl dhparam -out /etc/postfix/dh_512.pem2 -2 512

# change param
mv /etc/postfix/dh_4096.pem2 /etc/postfix/dh_4096.pem
mv /etc/postfix/dh_512.pem2 /etc/postfix/dh_512.pem

## restart postfix und dovecot
service postfix restart
service dovecot restart
