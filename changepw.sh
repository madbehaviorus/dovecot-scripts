#!/bin/bash

# dovecote change pw
# v0.3.1


## set colors
RED="\033[1;31m"
NOCOLOR="\033[0m"

# you have to set your domain name here
maillocaldomain="mail.localdomain"

# catch the first insert
user=$1



################################### 
#
# functions
#
###################################

###############################
## begin function helper
function helper(){
        echo ""
        echo ""
        echo "-----------------------------------------------------------------------------------"
        echo "-----------------------------------------------------------------------------------"
        echo "Hilfe: --help / -h "
        echo "-----------------------------------------------------------------------------------"
        echo "1) Insert username:    -u \$user"
        echo "-----------------------------------------------------------------------------------"
        echo "2) Insert hash: -hs \$hash"
        echo ""
        echo "Possebilities for hashes: "
            echo ""
            echo "PLAIN     : PLAIN sum of the password stored in plaintext"
            echo "SHA       : SHA1 sum of the password stored in base64"
            echo "SSHA      : Salted SHA1 sum of the password stored in base64"
            echo "SHA256    : SHA256 sum of the password stored in base64. (v1.1 and later)"
            echo "SSHA256   : Salted SHA256 sum of the password stored in base64. (v1.2 and later)"
            echo "SHA512    : SHA512 sum of the password stored in base64. (v2.0 and later)"
            echo "SSHA512   : Salted SHA512 sum of the password stored in base64. (v2.0 and later)"
        echo "-----------------------------------------------------------------------------------"
        echo "3) Password :    -p \$password"
        echo "-----------------------------------------------------------------------------------"
        echo "-----------------------------------------------------------------------------------"
        echo " Default order for the inserts:"
        echo " 1) username"
        echo " 2) hash"
        echo " 3) password"
        echo ""
        echo "for example: /bin/bash new-user.sh -u \$username -hs \$hash -p \$password"
        echo ""
        echo "Note: you can also switch the inserts for 1-3"
        echo "-----------------------------------------------------------------------------------"
        echo ""
}
## end function helper
###############################


###############################
## begin function manualuser

function manualuser(){

    # ask for username
    echo "From which user do you want to change the password?"
    echo "Enter the name like \$user without @mail.localdomain."
    read user
    echo

    # vars for while 1
    twhile=2;
    i=1;

    ## while 1 for choosing algorythmic

    while [ "$twhile" = "2" ]
    do
            

        echo "Choose your hash algorythmic: "
        echo "-----------------------------------------------------------------------------------"
        echo "-0- PLAIN: PLAIN sum of the password stored in plaintext"
        echo "-1- SHA: SHA1 sum of the password stored in base64"
        echo "-2- SSHA: Salted SHA1 sum of the password stored in base64"
        echo "-3- SHA256: SHA256 sum of the password stored in base64. (v1.1 and later)"
        echo "-4- SSHA256: Salted SHA256 sum of the password stored in base64. (v1.2 and later)"
        echo "-5- SHA512: SHA512 sum of the password stored in base64. (v2.0 and later)"
        echo "-6- SSHA512: Salted SHA512 sum of the password stored in base64. (v2.0 and later)"
        echo "-----------------------------------------------------------------------------------"
        echo -n "Your choose: "
        read algowahl
        echo "-----------------------------------------------------------------------------------"
        
        if [[ "$algowahl" =~ ^[1-6]+$ ]] 
        then
            echo "Integer Numbers from 1-6 only"
            echo "Are you shure, to use the following algorythmic: " $algowahl "? -  y/n"
            read ttrue2
            echo "-----------------------------------------------------------------------------------"
            if [ "$ttrue2" = y ]
            then
                twhile=1;
            else
                if (( $i <= 7 ))
                then
                    twhile=2;
                    echo "1"
                else
                    algowahl=6;
                    echo ""
                    echo "To many trys"
                    echo ""
                    echo "Set the algorythmus to option -6- SSHA512: Salted SHA512 (recommend)"
                    echo ""
                    twhile=1;
                fi
            fi
        else
            echo ""
            echo "Integer Numbers from 1-6 only";
            echo "Please try again"
            echo ""
        fi
        i=$(($i+1));
    done



    # var for while 2
    var=2

    ## while 2 for choosing and set the password
    while [ "$var" = "2" ]
    do
            pw1="";
            echo "Please set the new password of the new user:"
            while IFS= read -r -s -n1 char; 
            do
                [[ -z $char ]] && { printf '\n'; break; } # ENTER pressed; output \n and break.
                if [[ $char == $'\x7f' ]]; 
                then # backspace was pressed
                    # Remove last char from output variable.
                    [[ -n $pw1 ]] && pw1=${pw1%?}
                    # Erase '*' to the left.
                    printf '\b \b' 
                else
                    # Add typed char to output variable.
                    pw1+=$char
                    # Print '*' in its stead.
                    printf '*'
                fi
            done
            # end enter pw1 Schleife
            
            echo "-----------------------------------------------------------------------------------"
            pw2="";
            echo "Please confirm the new password of the new user:"
            while IFS= read -r -s -n1 char;
            do
                [[ -z $char ]] && { printf '\n'; break; } # ENTER pressed; output \n and break.
                if [[ $char == $'\x7f' ]]; 
                then # backspace was pressed
                    # Remove last char from output variable.
                    [[ -n $pw2 ]] && pw2=${pw2%?}
                    # Erase '*' to the left.
                    printf '\b \b' 
                else
                    # Add typed char to output variable.
                    pw2+=$char
                    # Print '*' in its stead.
                    printf '*'
                fi
            done
            # end enter pw2 Schleife
            
            echo "-----------------------------------------------------------------------------------"
            
            if  [ "$pw1" = "$pw2" ]
            then    
                    # set algowahl to the choosing algorythmic
                    sethash;                    
                    
                    # add new user
                    changeuser;
                    # break the while loop
                    var=1
            else    
                    # passwords are not the same
                    echo "Your passwords are not the same!"
                    echo "Would you set the pw again? y/n"
                    read var2
                    echo "-----------------------------------------------------------------------------------"
                    if [ "$var2"=y ]
                    then
                            # continue the while loop
                            var=2;
                    else
                            # break the while loop
                            var=1;
                    fi
            fi
    done
}

## end function manualuser
###############################


###############################
## begin function sethash

function sethash(){
    # set algowahl to the choosing algorythmic
    case "algowahl" in
        0)
            hash=$(printf "$pw1\n$pw2\n" | doveadm pw -s PLAIN)
            algo=PLAIN;;
        1)
            hash=$(printf  "$pw1\n$pw2\n" | doveadm pw -s SHA)
            algo=SHA;;
        2)
            hash=$(printf  "$pw1\n$pw2\n" | doveadm pw -s SHA-CRYPT)
            algo=SHA-CRYPT;;
        3)
            hash=$(printf "$pw1\n$pw2\n" | doveadm pw -s SHA256)
            algo=SHA256;;
        4)
            hash=$(printf "$pw1\n$pw2\n" | doveadm pw -s SHA256-CRYPT)
            algo=SHA256-CRYPT;;
        5)
            hash=$(printf "$pw1\n$pw2\n" | doveadm pw -s SHA512)
            algo=SHA512;;
        6)
            hash=$(printf  "$pw1\n$pw2\n" | doveadm pw -s SHA512-CRYPT)
            algo=SHA512-CRYPT;;
        esac;
}

## end function sethash
###############################



###############################
## begin function changeuser

function changeuser(){
    # set the passord in /etc/dovecot/users
    echo "Set the new password"
    sed -i "s/$user\@.*/$user@$maillocaldomain:{$algo}$hash:10000:10000::\/srv\/vmail\/$maillocaldomain\/$user::/g" /etc/dovecot/users
    echo "-----------------------------------------------------------------------------------"
    echo "Password from $user has changed!"
    echo "-----------------------------------------------------------------------------------"
}

## end function changeuser
###############################







######################################################################
#
# main
#
######################################################################


## catch multiple inserts and compare it
if [ user = "--help" ] || [ user = "-h" ]
then
    helper;
else
    if [ $1 != "" ]
    then
        if [ $user != $3 ] && [ $user != $5 ] && [ $3 != $5 ]
        then
            case "$user" in
                -u) user=$2;;
                -hs) algowahl=$2;;
                -pw) pw=$2;;
                *)  echo "";
                    echo -e "${RED} $user is not valid with the options -u, -hs or -pw ${NOCOLOR}";
                    echo "";
                    helper; 
                    exit;
            esac
            case "$3" in
                -u) user=$4;;
                -hs) algowahl=$4;;
                -pw) pw=$4;;
                *)  echo "";
                    echo -e "${RED} $3 is not valid with the options -u, -hs or -pw ${NOCOLOR}";
                    echo "";
                    helper; 
                    exit;
            esac
            case "$5" in
                -u) user=$6;;
                -hs) algowahl=$6;;
                -pw) pw=$6;;
                *)  echo "";
                    echo -e "${RED} $5 is not valid with the options -u, -hs or -pw ${NOCOLOR}";
                    echo "";
                    helper; 
                    exit;
            esac
            
            # set the choosed hash
            sethash;
            # add the user to dovecote users
            changeuser;
        else
            echo "-----------------------------------------------------------------------------------"
            echo " You can not use doubled insert options, please try again:"
            echo "-----------------------------------------------------------------------------------"
            helper;
            exit;
        fi
    else
        manualuser;
    fi
fi




